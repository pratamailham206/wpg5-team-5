﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public Button playBtn;
    public Button settingBtn;
    public Button creditBtn;
    public Button exitBtn;

    void Start()
    {
        playBtn.onClick.AddListener(() => LoadScene("CharacterSelect"));
        settingBtn.onClick.AddListener(() => LoadScene("Settings"));
        creditBtn.onClick.AddListener(() => LoadScene("Credit"));
        exitBtn.onClick.AddListener(ExitGame);
    }

    void LoadScene(string _sceneName)
    {
        SceneManager.LoadScene(_sceneName);
        MusicManager.instance.buttonSFX.Play();
    }

    void ExitGame()
    {
        Application.Quit();
        MusicManager.instance.buttonSFX.Play();
    }
}
