﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadSceneManager : MonoBehaviour
{
    public Button backBtn;
    public string sceneName;
    void Start()
    {
        backBtn.onClick.AddListener(LoadToScene);
    }

    public void LoadToScene()
    {
        SceneManager.LoadScene(sceneName);
        MusicManager.instance.buttonSFX.Play();
    }
}
