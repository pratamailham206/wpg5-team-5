﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class Bom2Script : MonoBehaviour
{
    public float delay = 4f;
    public float radius = 5f;

    public float explosionForce = 500f;
    public GameObject explosionEffect;

    float countdown;
    bool hasExploded = false;

    Collider[] colliders;

    int scored;
    void Start()
    {
        countdown = delay;
        GetComponent<Arena2Manager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Arena2Manager.instance.started)
        {
            countdown -= Time.deltaTime;
            if (countdown <= 0f && !hasExploded)
            {
                Exploded();
                hasExploded = true;
            }
        }        
    }

    void Exploded()
    {
        Instantiate(explosionEffect, transform.position, transform.rotation);
        colliders = Physics.OverlapSphere(transform.position, radius);

        foreach (Collider nearbyObject in colliders)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(explosionForce, transform.position, radius);
            }
            if (nearbyObject.tag == "Player" || nearbyObject.tag == "Player2")
            {
                nearbyObject.GetComponent<Player2Movement>().SetStunned(nearbyObject.transform.forward * -1);
                scored = Random.Range(5, 15);
                nearbyObject.GetComponent<Player2Movement>().decreaseScore(scored);
            }
        }
        MusicManager.instance.explodeSFX.Play();
        Destroy(this.gameObject);
    }
}
