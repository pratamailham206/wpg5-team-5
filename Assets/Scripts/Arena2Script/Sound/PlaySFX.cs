﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySFX : MonoBehaviour
{
    public AudioSource SoundFX;

    void Start(){

    }

    // Start is called before the first frame update
    public void SFXPlay()
    {
        SoundFX.Play();
    }

    
}
