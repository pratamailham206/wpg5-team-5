﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MusicManager : MonoBehaviour
{
    private static MusicManager _instance;

    public float BGMVolume;
    public float SFXVolume;

    public AudioSource BGM1;
    public AudioSource BGM2;
    public AudioSource buttonSFX;
    public AudioSource punchSFX;
    public AudioSource explodeSFX;
    public AudioSource spawnSFX;
    public AudioSource GameOverSFX;
    public AudioSource takeSFX;
    public AudioSource readyStartSFX;
    public AudioSource tikSFX;
    public AudioSource[] auhSFX;

    void Start(){
        BGMVolume = 1;
        SFXVolume = 1;
        BGM1.Play();
    }

    public static MusicManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<MusicManager>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }

    void Update()
    {
        if(SceneManager.GetActiveScene().name == "Settings"){
            BGMVolume = GameObject.Find("Music-Slider").GetComponent<Slider>().value;
            SFXVolume = GameObject.Find("Sfx-Slider").GetComponent<Slider>().value;
            BGM1.volume = BGMVolume;
            BGM2.volume = BGMVolume;
            buttonSFX.volume = SFXVolume;
            punchSFX.volume = SFXVolume;
            explodeSFX.volume = SFXVolume;
            spawnSFX.volume = SFXVolume;
            GameOverSFX.volume = SFXVolume;
            takeSFX.volume = SFXVolume;
            readyStartSFX.volume = SFXVolume;
            tikSFX.volume = SFXVolume;
            auhSFX[0].volume = SFXVolume;
            auhSFX[1].volume = SFXVolume;
        }
    }

    public void Change()
    {
        if (BGM1.isPlaying == true)
        {
            BGM1.Stop();
            BGM2.Play();
        }
        else if (BGM2.isPlaying == true)
        {
            BGM2.Stop();
            BGM1.Play();
        }
    }
}
