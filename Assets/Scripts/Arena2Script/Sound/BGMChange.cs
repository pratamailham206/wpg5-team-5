﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMChange : MonoBehaviour
{

    private AudioSource BGM1;
    private AudioSource BGM2;

    private void Start() {
        BGM1 = GameObject.Find("BGM1").GetComponent<AudioSource>();
        BGM2 = GameObject.Find("BGM2").GetComponent<AudioSource>();
    }
    public void Change(){
        if(BGM1.isPlaying == true){
            BGM1.Stop();
            BGM2.Play();
        }
        else if(BGM2.isPlaying == true){
            BGM2.Stop();
            BGM1.Play();
        }
    }
}
