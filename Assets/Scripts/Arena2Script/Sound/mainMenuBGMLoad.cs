﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mainMenuBGMLoad : MonoBehaviour
{
    public static mainMenuBGMLoad mainMenuBGM;
    private void Awake() {
        if(mainMenuBGM == null){
            mainMenuBGM = this;
        }
        else if(mainMenuBGM != this){
            Destroy(gameObject);
        }
    }
}
