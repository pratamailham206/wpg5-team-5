﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SoundHandlerScript : MonoBehaviour
{
    public float BGMVolume;
    public float SFXVolume;

    public GameObject [] BGM;
    public GameObject [] SFX;

    // Start is called before the first frame update
    void Start()
    {
        BGMVolume = 1;
        SFXVolume = 1;
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if(SceneManager.GetActiveScene().name == "SettingScene"){
            BGMVolume = GameObject.Find("BGM").GetComponent<Slider>().value;
            SFXVolume = GameObject.Find("SFX").GetComponent<Slider>().value;
        }
        BGM = GameObject.FindGameObjectsWithTag("BGM");
        SFX = GameObject.FindGameObjectsWithTag("SFX");
        foreach(GameObject BGMAudio in BGM){
            BGMAudio.GetComponent<AudioSource>().volume = BGMVolume;
        }
        foreach(GameObject SFXAudio in SFX){
            SFXAudio.GetComponent<AudioSource>().volume = SFXVolume;
        }
    }
}
