﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BGM2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if(!(SceneManager.GetActiveScene().name == "Level1" || SceneManager.GetActiveScene().name == "Level2" || SceneManager.GetActiveScene().name == "Level3"|| SceneManager.GetActiveScene().name == "Level4"|| SceneManager.GetActiveScene().name == "Level5"|| SceneManager.GetActiveScene().name == "Level6")){
            GameObject.Find("BGM1").SetActive(true);
            this.gameObject.SetActive(false);
        }
    }

    public void playBGM(){
        this.gameObject.GetComponent<AudioSource>().Play();
    }
}
