﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Arena2Manager : MonoBehaviour
{
    public static Arena2Manager instance;

    float GameTime;
    public float xMinPos;
    public float zMinPos;
    public float xMaxPos;
    public float zMaxPos;

    private float randX;
    private float randZ;

    public Player2Movement player1;
    public Player2Movement player2;

    public GameObject[] randomitem;
    float startSpawnTime;
    float spawnTime;

    public static int itemSpawned;
    int randItem;

    Vector3 itemSpawnedIn;

    public Text player1Score;
    public Text player2Score;
    public Text TimesText;

    private int bomIndex = 1;

    public GameObject pauseModal;
    public Button pauseBtn;
    public Button resumeBtn;

    public GameObject winnerModal;
    public Text winText;
    public Text scoreWin;
    public Text loseText;
    public Text scoreLose;

    public bool started;
    public Renderer charaterRenderer1;
    public Renderer charaterRenderer2;

    public Button backToMenu;

    public GameObject bgStart;
    public Text countdownText;
    private int start;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
        started = false;
    }

    void Start()
    {
        winnerModal.SetActive(false);
        pauseModal.SetActive(false);
        startSpawnTime = 1f;
        spawnTime = 0;
        itemSpawned = 0;
        GameTime = 120f;
        start = 3;

        pauseBtn.onClick.AddListener(PauseGame);
        resumeBtn.onClick.AddListener(ResumeGame);
        backToMenu.onClick.AddListener(BackToMenu);

        charaterRenderer1.material.SetTexture("_BaseMap", CharacterState.instance.skinTextures[CharacterState.instance.chara1skinId]);
        charaterRenderer2.material.SetTexture("_BaseMap", CharacterState.instance.skinTextures[CharacterState.instance.chara2skinId]);

        StartCoroutine(countDownStart());
    }

    // Update is called once per frame
    void Update()
    {
        if (started)
        {
            SpawnTimeItem();

            player1Score.text = player1.score.ToString();
            player2Score.text = player2.score.ToString();

            countdownGameTime();
        }
    }

    public IEnumerator countDownStart()
    {
        bgStart.SetActive(true);
        MusicManager.instance.readyStartSFX.Play();
        countdownText.text = start.ToString();
        start--;
        yield return new WaitForSeconds(0.5f);
        countdownText.text = start.ToString();
        start--;
        yield return new WaitForSeconds(0.5f);
        countdownText.text = start.ToString();
        start--;
        yield return new WaitForSeconds(0.5f);
        countdownText.text = "Start";
        yield return new WaitForSeconds(0.5f);
        bgStart.gameObject.SetActive(false);
        started = true;
    }

    void SpawnTimeItem()
    {
        if (spawnTime <= 0)
        {
            if (itemSpawned < 20)
            {
                SpawnItem();
                spawnTime = startSpawnTime;
            }
        }

        else if (spawnTime > 0)
        {
            spawnTime -= 1 * Time.deltaTime;
        }
    }

    void countdownGameTime()
    {        
        if(GameTime <= 0)
        {
            GameTime = 0;
            TimesText.GetComponent<Text>().text = GameTime.ToString();

            if (player1.score > player2.score)
            {
                scoreWin.text = "score : " + player1.score.ToString();
                winText.text = "Player 1 Win";
                loseText.text = "Player 2";
                scoreLose.text = "score : " + player2.score.ToString();
            }
            else
            {
                scoreWin.text = "score : " + player2.score.ToString();
                winText.text = "Player 2 Win";
                loseText.text = "Player 1";
                scoreLose.text = "score : " + player1.score.ToString();
            }      
            winnerModal.SetActive(true);
            started = false;
            MusicManager.instance.GameOverSFX.Play();
        }
        else
        {
            GameTime -= 1 * Time.deltaTime;
            TimesText.GetComponent<Text>().text = GameTime.ToString();
        }
    }

    void SpawnItem()
    {
        randX = Random.Range(xMinPos, xMaxPos);
        randZ = Random.Range(zMinPos, zMaxPos);

        randItem = Random.Range(0, randomitem.Length);

        itemSpawnedIn = new Vector3(randX, 8.5f, randZ);

        if(randItem == bomIndex)
        {
            Instantiate(randomitem[randItem], itemSpawnedIn, Quaternion.identity);
        }
        else
        {
            Item2Script item = Instantiate(randomitem[randItem], itemSpawnedIn, Quaternion.identity).GetComponent<Item2Script>();

            int randomitemType = Random.Range(1, 3);
            item.gameObject.tag = "item" + randomitemType;

            if(item.gameObject.tag == "item1")
            {
                item.ChangeColor(255, 0, 0);
            }
            else
            {
                item.ChangeColor(246, 255, 0);
            }

            itemSpawned++;
        }
        MusicManager.instance.spawnSFX.Play();
    }

    public void PauseGame()
    {
        started = false;
        player1.canMove = false;
        player2.canMove = false;
        player1.agent.speed = 0;
        player2.agent.speed = 0;
        pauseModal.SetActive(true);
        MusicManager.instance.buttonSFX.Play();
    }

    public void ResumeGame()
    {
        started = true;
        player1.canMove = true;
        player2.canMove = true;
        player1.agent.speed = player1.agentOriginalSpeed;
        player2.agent.speed = player2.agentOriginalSpeed;
        pauseModal.SetActive(false);
        MusicManager.instance.buttonSFX.Play();
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("MainMenu");
        MusicManager.instance.buttonSFX.Play();
        MusicManager.instance.Change();
    }
}
