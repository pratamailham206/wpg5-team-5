﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SocialPlatforms.Impl;

public class Player2Movement : MonoBehaviour
{
    // Start is called before the first frame update
    public NavMeshAgent agent;
    public float agentOriginalSpeed;

    //PLAYER MOVE
    [SerializeField] private float speed = 3.5f;
    [SerializeField] private float desiredRotationSpeed = 0.1f;
    [SerializeField] private float allowPlayerRotation = 0.1f;

    private Vector3 desiredMoveDirection;

    private float inputX;
    private float inputZ;

    private Camera cam;

    public bool player1 = true;
    public bool player2 = false;

    public bool canMove;
    public bool stunned;

    public GameObject stunEffect;
    public Vector3 bashDirection;

    public float score;

    Collider[] hitObject;
    public Transform hitPost;
    private Animator anim;


    public void Awake()
    {
        anim = GetComponentInChildren<Animator>();
    }

    void Start()
    {
        canMove = true;
        stunned = false;
        cam = Camera.main;
        agent = GetComponent<NavMeshAgent>();
        agentOriginalSpeed = agent.speed;
        score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (stunned)
            StunMovement();
        else
        {
            DetectingInput();
        }
    }

    void StunMovement()
    {
        Vector3 newPosition = transform.position;
        NavMeshHit navhit;
        bool isValid = NavMesh.SamplePosition(newPosition, out navhit, .1f, NavMesh.AllAreas);

        if (!isValid)
            return;

        transform.position = navhit.position;
    }

    public void SetStunned(Vector3 dir)
    {
        bashDirection = dir;
        StartCoroutine(SetTargetStunned(4.75f));

        IEnumerator SetTargetStunned(float time)
        {
            anim.SetBool("Stun", true);
            canMove = false;
            stunned = true;
            Instantiate(stunEffect, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), transform.rotation);            
            agent.speed = 0;
            yield return new WaitForSeconds(time);
            anim.SetBool("Stun", false);
            agent.speed = agentOriginalSpeed;
            stunned = false;
            canMove = true;
        }
        int randomSFX = Random.Range(0, 2);
        MusicManager.instance.auhSFX[randomSFX].Play();
    }

    void DetectingInput()
    {
        if (player1 && canMove)
        {
            inputX = Input.GetAxis("Horizontal");
            inputZ = Input.GetAxis("Vertical");

            if (Input.GetKeyDown(KeyCode.Space))
            {
                hitPlayer();
            }

            float inputMagnitute = new Vector2(inputX, inputZ).sqrMagnitude;

            if (inputMagnitute > allowPlayerRotation)
            {
                PlayerMoveCharacter();
                anim.SetBool("IsWalk", true);
            }
            else
            {
                anim.SetBool("IsWalk", false);
            }
        }

        if (player2 && canMove)
        {
            inputX = Input.GetAxis("arrowH");
            inputZ = Input.GetAxis("arrowV");

            if (Input.GetKeyDown(KeyCode.P))
            {
                hitPlayer();
            }

            float inputMagnitute = new Vector2(inputX, inputZ).sqrMagnitude;

            if (inputMagnitute > allowPlayerRotation)
            {
                PlayerMoveCharacter();
                anim.SetBool("IsWalk", true);
            }
            else
            {
                anim.SetBool("IsWalk", false);
            }
        }        
    }

    void hitPlayer()
    {
        StartCoroutine(PunchPlayer());
        hitObject = Physics.OverlapSphere(this.transform.position, 1);
        MusicManager.instance.punchSFX.Play();
        foreach (Collider nearbyPlayer in hitObject)
        {
            if(player1)
            {
                if (nearbyPlayer.tag == "Player2")
                {
                    nearbyPlayer.GetComponent<Player2Movement>().SetStunned(nearbyPlayer.transform.forward * -1);
                    stunned = false;
                }
            }
            if (player2)
            {
                if (nearbyPlayer.tag == "Player")
                {
                    nearbyPlayer.GetComponent<Player2Movement>().SetStunned(nearbyPlayer.transform.forward * -1);
                    stunned = false;
                }
            }
        }
    }

    IEnumerator PunchPlayer()
    {
        anim.SetBool("Punch", true);
        canMove = false;
        agent.speed = 0;
        yield return new WaitForSeconds(0.75f);
        anim.SetBool("Punch", false);
        agent.speed = agentOriginalSpeed;
        canMove = true;
    }

    void PlayerMoveCharacter()
    {
        if (player1)
        {
            inputX = Input.GetAxis("Horizontal");
            inputZ = Input.GetAxis("Vertical");
        }

        if (player2)
        {
            inputX = Input.GetAxis("arrowH");
            inputZ = Input.GetAxis("arrowV");
        }

        Vector3 axis = new Vector3(inputX, 0, inputZ);

        var forward = cam.transform.forward;
        var right = cam.transform.right;

        forward.y = 0f;
        right.y = 0f;

        forward.Normalize();
        right.Normalize();

        desiredMoveDirection = forward * inputZ + right * inputX;

        if (axis.magnitude < .01f)
            return;

        Vector3 newPosition = transform.position + desiredMoveDirection * Time.deltaTime * speed;
        NavMeshHit hit;
        bool isValid = NavMesh.SamplePosition(newPosition, out hit, .3f, NavMesh.AllAreas);

        if (!isValid)
            return;

        if ((transform.position - hit.position).magnitude >= .02f)
            transform.position = hit.position;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), desiredRotationSpeed);
    }

    public void AddScore(float scoreAdded)
    {
        score += scoreAdded;
    }

    public void decreaseScore(float scoreDecreased)
    {
        score -= scoreDecreased;
        if(score < 0)
        {
            score = 0;
        }
    }
}
