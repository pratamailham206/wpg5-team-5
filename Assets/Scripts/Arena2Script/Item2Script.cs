﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item2Script : MonoBehaviour
{
    float addScore;
    public GameObject pickedEeffect;
    public SpriteRenderer labelSprite;

    public void ChangeColor(int r, int g, int b)
    {
        labelSprite.color = new Color(r, g, b);
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player" && this.gameObject.tag == "item1")
        {
            Instantiate(pickedEeffect, transform.position, transform.rotation);
            addScore = Random.Range(3, 5);
            Destroy(gameObject);
            collision.gameObject.GetComponent<Player2Movement>().AddScore(addScore);
            Arena2Manager.itemSpawned--;
            MusicManager.instance.takeSFX.Play();
        }

        if (collision.gameObject.tag == "Player2" && this.gameObject.tag == "item2")
        {
            Instantiate(pickedEeffect, transform.position, transform.rotation);
            addScore = Random.Range(3, 5);
            Destroy(gameObject);
            collision.gameObject.GetComponent<Player2Movement>().AddScore(addScore);
            Arena2Manager.itemSpawned--;
            MusicManager.instance.takeSFX.Play();
        }
    }
}
