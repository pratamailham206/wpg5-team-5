﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player4Movement : MonoBehaviour
{
    private NavMeshAgent agent;
    private float agentOriginalSpeed;

    //PLAYER MOVE
    [SerializeField] private float speed = 5;
    [SerializeField] private float desiredRotationSpeed = 0.1f;
    [SerializeField] private float allowPlayerRotation = 0.1f;

    private Vector3 desiredMoveDirection;

    private float inputX;
    private float inputZ;

    private Camera cam;

    public bool player1 = true;
    public bool player2 = false;

    public bool canMove;
    public bool stunned;

    public GameObject stunEffect;
    public Vector3 bashDirection;

    public float score;

    Collider[] catchCollider;

    public int BeeCatched;
    public int maxBeeCatced;
    void Start()
    {
        canMove = true;
        stunned = false;
        cam = Camera.main;
        agent = GetComponent<NavMeshAgent>();
        agentOriginalSpeed = agent.speed;
        score = 0;
        BeeCatched = 0;
        maxBeeCatced = 5;
    }

    void Update()
    {
        DetectingInput();

        if (stunned)
            StunMovement();
    }

    void StunMovement()
    {
        Vector3 newPosition = transform.position + bashDirection * Time.deltaTime * 1.5f;
        NavMeshHit navhit;
        bool isValid = NavMesh.SamplePosition(newPosition, out navhit, .3f, NavMesh.AllAreas);

        if (!isValid)
            return;

        transform.position = navhit.position;
    }

    public void SetStunned(Vector3 dir)
    {
        bashDirection = dir;
        StartCoroutine(SetTargetStunned(1.2f));

        IEnumerator SetTargetStunned(float time)
        {
            GetComponent<Player4Movement>().canMove = false;
            GetComponent<Player4Movement>().stunned = true;
            Instantiate(stunEffect, new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z), transform.rotation);
            agent.speed = 0;
            yield return new WaitForSeconds(time);
            agent.speed = agentOriginalSpeed;
            GetComponent<Player4Movement>().stunned = false;
            GetComponent<Player4Movement>().canMove = true;
        }
    }

    void DetectingInput()
    {
        if (player1 && canMove)
        {
            inputX = Input.GetAxis("Horizontal");
            inputZ = Input.GetAxis("Vertical");

            if (Input.GetKeyDown(KeyCode.Space))
            {                
                CatchBee();
            }

            float inputMagnitute = new Vector2(inputX, inputZ).sqrMagnitude;

            if (inputMagnitute > allowPlayerRotation)
            {
                PlayerMoveCharacter();
            }
        }

        if (player2 && canMove)
        {
            inputX = Input.GetAxis("arrowH");
            inputZ = Input.GetAxis("arrowV");

            if (Input.GetKeyDown(KeyCode.P))
            {
                Debug.Log("Hit");
            }

            float inputMagnitute = new Vector2(inputX, inputZ).sqrMagnitude;

            if (inputMagnitute > allowPlayerRotation)
            {
                PlayerMoveCharacter();
            }
        }
    }

    void CatchBee()
    {
        catchCollider = Physics.OverlapSphere(this.transform.position, 1.5f);
        foreach (Collider nearbyPlayer in catchCollider)
        {
            if(nearbyPlayer.tag == "Bee")
            {
                if (BeeCatched <= maxBeeCatced) {
                    nearbyPlayer.GetComponent<BeeAIMovement>().BeeCatched();
                    BeeCatched++;
                    Debug.Log(BeeCatched);
                }
            }
        }
    }

    void PlayerMoveCharacter()
    {
        if (player1)
        {
            inputX = Input.GetAxis("Horizontal");
            inputZ = Input.GetAxis("Vertical");
        }

        if (player2)
        {
            inputX = Input.GetAxis("arrowH");
            inputZ = Input.GetAxis("arrowV");
        }

        Vector3 axis = new Vector3(inputX, 0, inputZ);

        var forward = cam.transform.forward;
        var right = cam.transform.right;

        forward.y = 0f;
        right.y = 0f;

        forward.Normalize();
        right.Normalize();

        desiredMoveDirection = forward * inputZ + right * inputX;

        if (axis.magnitude < .01f)
            return;

        Vector3 newPosition = transform.position + desiredMoveDirection * Time.deltaTime * speed;
        NavMeshHit hit;
        bool isValid = NavMesh.SamplePosition(newPosition, out hit, .3f, NavMesh.AllAreas);

        if (!isValid)
            return;

        if ((transform.position - hit.position).magnitude >= .02f)
            transform.position = hit.position;

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), desiredRotationSpeed);
    }

    public void AddScore(float scoreAdded)
    {
        score += scoreAdded;
    }

    public void decreaseScore(float scoreDecreased)
    {
        score -= scoreDecreased;
    }

    public void CollectBee()
    {
        score += BeeCatched;
        BeeCatched = 0;
        Debug.Log("Bee Collected : " + score);
    }
}
