﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeAIMovement : MonoBehaviour
{
    public Transform[] waypoints;
    public GameObject player;

    public float speed;
    public float initialSpeed;

    private int indexWaypoints;
    private float direction;

    float DistancePlayer;
    void Start()
    {
        initialSpeed = speed;
        indexWaypoints = Random.Range(0, waypoints.Length - 1);
        transform.LookAt(waypoints[indexWaypoints].position);
        GetComponent<Arena4Manager>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        DistancePlayer = Vector3.Distance(transform.position, player.transform.position);

        if (DistancePlayer < 5)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, -speed * Time.deltaTime);
        }

        else if (DistancePlayer >= 5 && DistancePlayer <=6 )
        {
            StartCoroutine(DecreaseIndex());                       
        }

        else
        {
            direction = Vector3.Distance(transform.position, waypoints[indexWaypoints].position);
            if (direction < 1f)
            {
                StartCoroutine(delayMove());
                IncreaseIndex();
            }

            transform.position = Vector3.MoveTowards(transform.position, waypoints[indexWaypoints].position, speed * Time.deltaTime);
        }
    }

    IEnumerator delayMove()
    {
        speed = 0;
        yield return new WaitForSeconds(2);
        speed = initialSpeed;
    }

    void IncreaseIndex()
    {
        indexWaypoints++;
        if(indexWaypoints >= waypoints.Length)
        {
            indexWaypoints = 0;
        }
        transform.LookAt(waypoints[indexWaypoints].position);
    }

    IEnumerator DecreaseIndex()
    {
        indexWaypoints--;
        if (indexWaypoints < 0)
        {
            indexWaypoints = waypoints.Length - 1;
        }
        yield return new WaitForSeconds(1);
        transform.LookAt(waypoints[indexWaypoints].position);
        transform.position = Vector3.MoveTowards(transform.position, waypoints[indexWaypoints].position, speed * Time.deltaTime);
    }

    public void BeeCatched()
    {
        Arena4Manager.BeeSpawned--;
        Destroy(this.gameObject);        
    }
}
