﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arena4Manager : MonoBehaviour
{
    public GameObject Bee;
    public static int BeeSpawned;
    public Transform[] startSpawnPoints;
    int spawnPointIndex;

    float startSpawnDur;
    float spawnDur;
    void Start()
    {
        startSpawnDur = 0;
        spawnDur = 3;
        BeeSpawned = 0;
        spawnPointIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(startSpawnDur <= 0)
        {
            if(BeeSpawned < 10)
            {
                SpawnBee();
                startSpawnDur = spawnDur;
            }
        }

        else if(startSpawnDur > 0)
        {
            startSpawnDur -= 1 * Time.deltaTime;
        }
    }

    void SpawnBee()
    {
        spawnPointIndex = Random.Range(0, startSpawnPoints.Length);
        Instantiate(Bee, startSpawnPoints[spawnPointIndex].position, Quaternion.identity);
        BeeSpawned++;
    }
}
