﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CageScript : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform[] patrolPos;
    Collider[] CageCol;

    public float speed;
    float direction;

    int indexPatrol;
    void Start()
    {
        indexPatrol = 0;
    }

    // Update is called once per frame
    void Update()
    {
        PatrolCage();
        DetectColPlayer();
    }

    void PatrolCage()
    {
        direction = Vector3.Distance(transform.position, patrolPos[indexPatrol].position);
        if (direction < 1f)
        {           
            IncreaseIndex();
        }
        transform.position = Vector3.MoveTowards(transform.position, patrolPos[indexPatrol].position, speed * Time.deltaTime);        
    }

    void DetectColPlayer()
    {
        CageCol = Physics.OverlapSphere(transform.position, 5f);
        foreach(Collider player in CageCol)
        {
            Debug.Log(player);
            if (player.tag == "Player")
            {                
                if(player.gameObject.GetComponent<Player4Movement>().BeeCatched > 0)
                {
                    player.gameObject.GetComponent<Player4Movement>().CollectBee();
                }                
            }
        }
    }

    void IncreaseIndex()
    {
        indexPatrol++;
        if (indexPatrol >= patrolPos.Length)
        {
            indexPatrol = 0;
        }        
    }
}
