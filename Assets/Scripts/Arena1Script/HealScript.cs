﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealScript : MonoBehaviour
{
    public float delay = 10f;
    public float radius = 5f;

    public GameObject healEffect;

    public GameObject playerPos;
    public Transform playerP;

    float countdown;
    bool hasHealed;

    Collider[] colliders;

    public bool isHealPicked;
    public Rigidbody rb;

    int scored;
    void Start()
    {
        hasHealed = false;
        countdown = delay;
        isHealPicked = false;
        rb = this.gameObject.GetComponent<Rigidbody>();
        GetComponent<Arena1Manager>();
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0f && !hasHealed)
        {
            Healed();
            hasHealed = true;
        }
        if (isHealPicked)
        {
            healPicked(playerPos, playerP);
        }

        else if (!isHealPicked)
        {
            this.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        }
    }

    public void healPicked(GameObject player, Transform playerpos)
    {
        this.transform.position = playerpos.transform.position;
        playerPos = player;
        rb.velocity = Vector3.zero;
    }

    void Healed()
    {
        Instantiate(healEffect, transform.position, transform.rotation);
        colliders = Physics.OverlapSphere(transform.position, radius);

        foreach (Collider nearbyObject in colliders)
        {
            if (nearbyObject.tag == "Base")
            {
                nearbyObject.GetComponent<BaseScript>().HealBase(20);
                scored = Random.Range(15, 25);
                if (playerPos != null)
                {
                    playerPos.GetComponent<Player1Movement>().addScore(scored);
                    Debug.Log("Healed");
                }
            }
        }

        Destroy(this.gameObject);
        Arena1Manager.healSpawned--;
        Debug.Log(Arena1Manager.healSpawned);
    }
}
