﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class BomScript : MonoBehaviour
{
    public float delay = 8.5f;
    public float radius = 5f;

    public float explosionForce = 500f;
    public GameObject explosionEffect;

    public GameObject playerPos;
    public Transform playerP;

    float countdown;
    bool hasExploded = false;

    Collider[] colliders;

    public bool isBomPicked;
    public Rigidbody rb;

    int scored;
    void Start()
    {
        countdown = delay;
        isBomPicked = false;
        rb = this.gameObject.GetComponent<Rigidbody>();
        GetComponent<Arena1Manager>();
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if(countdown <= 0f && !hasExploded)
        {
            Exploded();
            hasExploded = true;
        }
        if (isBomPicked)
        {
            bomPicked(playerPos, playerP);            
        }

        else if (!isBomPicked)
        {
            this.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        }
    }

    public void bomPicked(GameObject player, Transform playerpos)
    {
        this.transform.position = playerpos.transform.position;
        playerPos = player;
        rb.velocity = Vector3.zero;
    }

    void Exploded()
    {
        Instantiate(explosionEffect, transform.position, transform.rotation);
        colliders = Physics.OverlapSphere(transform.position, radius);

        foreach (Collider nearbyObject in colliders)
        {
            Rigidbody rb = nearbyObject.GetComponent<Rigidbody>();
            if(rb != null)
            {
                rb.AddExplosionForce(explosionForce, transform.position, radius);
            }
            if(nearbyObject.tag == "Base")
            {
                nearbyObject.GetComponent<BaseScript>().baseDamaged(20);
                scored = Random.Range(15, 25);
                if(playerPos != null)
                {
                    playerPos.GetComponent<Player1Movement>().addScore(scored);                   
                }
            }
            if(nearbyObject.tag == "Player")
            {
                nearbyObject.GetComponent<Player1Movement>().SetStunned(nearbyObject.transform.forward * -1);
                scored = Random.Range(5, 15);
                nearbyObject.GetComponent<Player1Movement>().decreaseScore(scored);
            }
        }

        Destroy(this.gameObject);
        Arena1Manager.bomSpawned--;
    }
}
